
import Foundation

struct Property {
    var address:String
    var latitude:Float
    var longitude:Float
}

class Houses {
    
    class func getProperties(lat: Float, lon: Float, completion: (properties:Array<Property>)->()) {
        var propertyArray = [Property]()
        println("lat: \(lat) lon: \(lon)")
        let spread:Double = 0.2
        let lat_min = lat - 0.2
        let lat_max = lat + 0.2
        let lon_min = lon - 0.2
        let lon_max = lon + 0.2
        let url = NSURL(string: "http://api.zoopla.co.uk/api/v1/property_listings.json?lat_min=\(lat_min)&lat_max=\(lat_max)&lon_min=\(lon_min)&lon_max=\(lon_max)&api_key=z6e6ksbp8mejpqtrwhy8ts4k")
        println(url)
        if let tempUrl:NSURL = url {
            let urlReq = NSURLRequest(URL: tempUrl)
            let queue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(urlReq, queue: queue, completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if (error != nil) {
                    println("API error: \(error), \(error.userInfo)")
                }
                var jsonError:NSError?
                var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                if (jsonError != nil) {
                    println("Error parsing json: \(jsonError)")
                }
                else {
                    var newProperty = Property(address: "", latitude: 0.0, longitude: 0.0)
                    //println(json)
                    if let properties:Array<AnyObject> = json["listing"] as? Array<AnyObject> {
                        for property in properties {
                            if let address = property["agent_address"] as? String {
                                //println("Address: \(address)")
                                newProperty.address = address
                            }
                            if let lat = property["latitude"] as? Float {
                                //println("Lat: \(lat)")
                                newProperty.latitude = lat
                            }
                            if let lon = property["longitude"] as? Float {
                                //println("Lon: \(lon)")
                                newProperty.longitude = lon
                            }
                            propertyArray.append(newProperty)
                        }
                    }
                }
                completion(properties: propertyArray)
            })
        }
    }
    
}
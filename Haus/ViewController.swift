
import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    let location = CLLocationManager()
    var propertyArray = [Property]()

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        CLLocationManager.locationServicesEnabled()
        self.location.requestWhenInUseAuthorization()
        
        self.location.delegate = self
        self.location.distanceFilter = 100
        self.location.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.location.startUpdatingLocation()
    }
    
    func addPins() {
        for house in self.propertyArray {
            println(house.address)
            let location = CLLocationCoordinate2D(
                latitude: Double(house.latitude),
                longitude: Double(house.longitude)
            )
            let annotation = MKPointAnnotation()
            annotation.setCoordinate(location)
            annotation.title = "For Sale"
            annotation.subtitle = house.address
            self.mapView.addAnnotation(annotation)
        }
        //let span = MKCoordinateSpanMake(0.05, 0.05)
        //let region = MKCoordinateRegion(center: self.mapView.userLocation.coordinate, span: span)
        //mapView.setRegion(region, animated: true)
        
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        println("newLocation")
        if let loc = locations.last as? CLLocation {
            let latLon = loc.coordinate
            println("lat: \(latLon.latitude) lon: \(latLon.longitude)")
            Houses.getProperties(Float(latLon.latitude), lon:Float(latLon.longitude),
                completion: {(properties: Array<Property>) in
                println("DONE!!!")
                    
                //for house in properties {
               //     println(house.address)
                //}
                    
                self.propertyArray = properties
                self.addPins()
                let center = CLLocationCoordinate2D(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
                self.mapView.setRegion(region, animated: true)
            })
        }
    }

    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("ERROR")
    }
    
    
    override func viewWillAppear(animated: Bool) {
        println("viewWillAppear")
        self.location.startUpdatingLocation()
    }
    
    override func viewWillDisappear(animated: Bool) {
        println("viewWillDisappear")
        self.location.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}









